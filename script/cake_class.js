let obj = JSON.parse(
  $response.body
    .replace(/\"freeBadge\":\s?\w+/g, '"freeBadge":true')
    .replace(/\"available\":\s?\w+/g, '"available":true')
    .replace(
      /\"restrictedAfterFreeTrial\":\s?\w+/g,
      '"restrictedAfterFreeTrial":false'
    )
    .replace(/\"restrictedNow\":\s?\w+/g, '"restrictedNow":false')
    .replace(/\"membershipOnly\":\s?\w+/g, '"membershipOnly":false')
    .replace(
      /\"membershipOnlyPlaylist\":\s?\w+/g,
      '"membershipOnlyPlaylist":false'
    )
    .replace(
      /\"membershipOnlySentence\":\s?\w+/g,
      '"membershipOnlySentence":false'
    )
);

$done({ body: JSON.stringify(obj) });
