const body = $response.body
  .replace(/\"is_pro\":\s*\w+/g, '"is_pro":true')
  .replace(/\"is_premium\":\s*\w+/g, '"is_premium":true');

$done({ body });
