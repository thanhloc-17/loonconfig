const obj = JSON.parse($response.body);

obj.tier = { code: "pro", start_at: 1709808319, expire_at: null };

// obj.entitlements.features = [
//   {
//     code: "elsa_speak.feature.ai_role_play",
//     start_at: 1708879761,
//     expire_at: null,
//     credit_total: 10000,
//     application: "elsa_speak",
//   },
//   {
//     code: "speech_analyzer.feature.speech_analyzer",
//     start_at: 1708879761,
//     expire_at: null,
//     credit_total: 10000,
//     application: "speech_analyzer",
//   },
// ];
// obj.transactions = [
//   {
//     order_id: "FRE01FBA7JP..0",
//     catalog: "elsa_speak",
//     store: "voucher",
//     duration: "lifetime",
//     created_at: 1709808319,
//     updated_at: 1709868319,
//     interval: "onetime",
//     rf_id: "FRE01FBA7JP",
//     status: "granted",
//   },
// ];
obj.entitlements = {
  memberships: [
    {
      code: "elsa_speak.membership.elsa_speak",
      start_at: 1709808319,
      expire_at: null,
      credit_total: null,
      application: "elsa_speak",
    },
  ],
  features: [
    {
      code: "elsa_speak.feature.ai_role_play",
      start_at: 1709808319,
      expire_at: null,
      credit_total: 10000,
      application: "elsa_speak",
    },
    {
      code: "speech_analyzer.feature.speech_analyzer",
      start_at: 1709808319,
      expire_at: null,
      credit_total: 10000,
      application: "speech_analyzer",
    },
  ],
  contents: [],
};

const body = JSON.stringify(obj);

$done({ body });
