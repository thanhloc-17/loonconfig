var body = JSON.parse($response.body);
body.plan = 'Premium';
body.planLimits = {
    tokens: 9999,
    conversations: 9999,
    cues: 9999,
};
body.premiumDayState = {
    premiumDays: 9999
}

$done({ body: JSON.stringify(body) });