let obj = JSON.parse(
  $response.body
    .replace(/NONE/g, "RESTRICTED")
    .replace(/\"isEnrolled\":\s?\w+/g, '"isEnrolled":true')
    .replace(/\"isGifted\":\s?\w+/g, '"isGifted":false')
    .replace(/\"following\":\s?\w+/g, '"following":true')
);

// 获取请求地址
let requestUrl = $request.url;
if (/^https:\/\/api\.hello\.ef\.com\/graphql?/.test(requestUrl)) {
  if (obj?.data?.viewer?.me?.premium) {
    let premium = {
      planType: "yearly",
      __typename: "Premium",
      paymentProviderName: "APPLE",
      endTime: 7956886942,
      isEnabled: true,
      isFamilyLeader: true,
    };
    // obj.data.viewer.me.hasUsedRoleplay = true;
    obj.data.viewer.me.premium = premium;
  }

  obj?.data?.viewer?.lockedInBucketAssignments?.forEach((el) => {
    if (["hidden", "disabled", "visible"].includes(el.bucket))
      el.bucket = "enabled";
  });

  obj?.data?.viewer?.allBucketAssignments?.forEach((el) => {
    if (["hidden", "disabled", "visible"].includes(el.bucket))
      el.bucket = "enabled";
  });

  obj?.data?.node?.lessons?.forEach((el) => {
    if (el?.progress) el.progress.isCompleted = true;
  });

  // if (
  //   obj.data.hasOwnProperty("node") &&
  //   obj.data.node.hasOwnProperty("sections") &&
  //   obj.data.node.sections.length
  // ) {
  //   obj.data.node.sections.forEach((el, index) => {
  //     if (index > XiaoMaoIndex - 2) {
  //       el.progress.isCompleted = false;
  //     }
  //   });
  // }
}

$done({ body: JSON.stringify(obj) });
