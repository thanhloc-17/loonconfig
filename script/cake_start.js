let obj = JSON.parse($response.body);

obj.extra = {
    "subscriptionStatus" : "PLUS",
    "membershipSource" : "CAKE",
    "membership" : "PLUS"
  };

$done({body: JSON.stringify(obj)});
