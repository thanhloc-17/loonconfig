let body = JSON.parse($response.body);

if (body?.expiry_date === null) body.expiry_date = '2055-01-01';
if (body?.data?.expired_day === null) body.data.expired_day = '2055-01-01';

$done({
  body: JSON.stringify(body),
});
